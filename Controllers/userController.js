const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../Models/coursesSchema.js")



// Controllers


//This controller will create or register a user on our db/database
module.exports.userRegistration = (request, response) => {

		const input = request.body;

		User.findOne({email: input.email})
		.then(result => {
			if(result !== null){
				return response.send("The email is already taken!")
			}else{
				let newUser = new User({
					firstName: input.firstName,
					lastName: input.lastName,
					email: input.email,
					password: bcrypt.hashSync(input.password, 10),
					mobileNo: input.mobileNo
				})


				//save to database
				newUser.save()
				.then(save => {
					return response.send("You are now registered to our website!")
				})
				.catch(error => {
					return response.send(error)
				})





			}

		})
		.catch(error => {
			return response.send(error)
		})
}

//User Authentication
module.exports.userAuthentication = (request, response) => {
		let input = request.body;

		//Possible scenarios in logging in
			//1. email is not yet registered.
			//2. email is registered but the password is wrong

		User.findOne({email: input.email})
		.then(result => {
			if(result === null){
				return response.send("Email is not yet registered. Register first before logging in!")
			}else{
				// we have to verify if the passwor is correct
				// The "compareSync" method is used to compare a non encrypted password to the encrypted password.
				//it returns boolean value, if match true value will return otherwise false.
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return response.send({auth: auth.createAccessToken(result)});
				}else{
					return response.send("Password is incorrect!")
				}

			}




		})
		.catch(error => {
			return response.send(error);
		})

}

// Retrieve the user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the result document to an empty string("").
	3. Return the result back to the frontend
*/


module.exports.getProfile = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  console.log(userData);

  return User.findOne({ _id: userData._id }).then(result => {
    result.password = "";

    return response.send(result);
  });
};


//controller user enrollment
	//1. we can get the id of the user by getting decoding jwt
	//2. We can get the courseIDd by using the reques params

module.exports.enrollCourse = async (request, response) =>{
	//first we have to get the userId and the courseId

	//decode the token to extract the payload

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;





	//things that we need to do in this controller
		//first to push the courseId in the enrollments property of the user
		//second, to push the userId in the enrollees property
	 let isUserUpdated = await User.findById(userData._id)
	.then (result => {
		console.log(result)
		if(result === null){
			return false
		// } else {
		// 	Course.findById(courseId)
		// 	.then(result =>{
		// 		if(result === null){
		// 			return false
				}else{
					result.enrollments.push({courseId: courseId})
					return result.save()
					.then(save => true)
					.catch(error => false)
				}
			})

	let isCourseUpdated = await Course.findById(courseId).then(result =>{
		console.log(result)
		if(result === null){
			return false
		} else {
			result.enrollees.push({userId: userData._id})

			return result.save()
			.then(save => true)
			.catch(error => false)
		}
	})

	//s41 activity
	if(userData.isAdmin !== false){
		return response.send("Admin not allowed to access this page.")
	} else{
		const course = Course.findById(courseId)
		.then(result =>{
			if(!course){
				return response.send("Error 404 course not found")
			}
		})
	}

	if(isCourseUpdated && isUserUpdated){
		return response.send("The course is now enrolled!")

	}else{
		return response.send("There was an error during enrollment. Please try again")
	}

}
