const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js")

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) =>{
	let input = request.body;
	let userData =auth.decode(request.headers.authorization)

	let newCourse = new Course({
		name: input.name,
		description: input.description,
		price: input.price
	});

	// saves the created object to our database
	if(userData.isAdmin){
		// saves the created object to our database
		return newCourse.save()
		// course succesfully created
		.then(course =>{
			console.log(course);
			response.send(course);
		})
		// course creation failed
		.catch(error =>{
			console.log(error);
			response.send(false);
		})
	}else{
		return response.send("You are not an admin!")
	}
	
}


// Create a controller where in it will retrieved all the courses

module.exports.allCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	if(!userData.isAdmin){
		return response.send("You dont have access to this route!")
		}else{

			Course.find({})
			.then(result => response.send(result))
			.catch(error => response.send(error))
	}
}

// Crete a controller wherein it will retrieve course that are active

module.exports.allActiveCourses = (request, response) => {
	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error))
}


module.exports.allActiveCourses = (request, response) => {
const userData = auth.decode(request.headers.authorization)
if(!userData.isAdmin){
	return response.send("Your not an admin");
	}else{
	Course.find({isActive: false})
	.then(result => response.send(result))
	.catch(error => response.send(error))
 }
}

//this controller will get the details of specific course

module.exports.courseDetails = (request, response) => {
	// to get the params from the url
	const courseId = request.params.courseId;

	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error))

}

//this controller is for updating a specific course

/*
		Business logic:
			1.We are going to edit/update the course, that is in the params.

*/
module.exports.updateCourse = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	const input = request.body;


	if(!userData.isAdmin){
		return response.send("You don't have acces in this page!")
	}else{
	
	await Course.findOne({_id: courseId})
		.then(result =>{
			if(result === null){
				return response.send("courseId is invalid, please try again!")
			}else{
				let updatedCourse= {
					name: input.name,
					description: input.description,
					price: input.price
				}
				
				Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
				.then(result => {
					console.log(result);
					return response.send(result)})
				.catch(error => response.send(error));
			}
		})
		
	}
}

module.exports.archiveCourse = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	const isActive = request.body.isActive;


	if(!userData.isAdmin){
		return response.send("You don't have acces in this page!")
	} else {
	
	 const course = await Course.findOne({_id: courseId})
		.then(result =>{
			if(result === null){
				return response.send("courseId is invalid, please try again!")
			} else {
	const archivedCourse = { isActive };
				
				Course.findByIdAndUpdate(courseId, archivedCourse, {new: true})
				.then(result => {
					console.log(result);
					return response.send(result)})
			}
		})
		
	}
}






