const mongoose = require("mongoose");
const coursesSchema = mongoose.Schema({
	
	name:{
		type: String,
		required:[true, "Name of the course is required!"]
	},
	description:{
		type: String,
		required: [true, "Description of the course is required!"]
	},
	price:{
		type: Number,
		required: [true, "Price of the course is required!"]
	},
	isActive:{
		type: Boolean,
	default: true
	},
	created:{
		type: Date,
		// the ne date expression instantiates a new "date" that stores the current data and time whenever a couse is updated in our database
		default: new Date()
	},
	enrollees: [
			{
				userId: {
					type: String,
					required: [true, "UserId is required!"]
				},
				enrolledOn:{
					type: Date,
				default: new Date()
				}
			}
		]
})

module.exports = mongoose.model("Course", coursesSchema);