// [SECTION] bcrypt
	// npm i bcrypt
	//in our application we will be using this package to demonstrat how to encryp data when a user register to our website

	//the "bcrypt" package is one of the many packages that we can use to encrypt information but it is not commonly recommended because of how simple the algo is.

	//There are other more advanced encryption packages that can be used.

	//syntax for hashing password
		// Syntax:
			// bcrypt.hashSync(password, saltRounds)
			//saltRounds is the value provided as the number of "salt" round that the bcrypt algorithm will run in order to encrypt the password

//[SECTION] JWT - JSONwebtoken package

	//JSON web tokens is an industry standard for sending information between our apps in a secure manner
	//the json webtoken package will us to gain access to methods that will help us create JSON web token