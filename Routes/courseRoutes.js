const express = require("express");
const router = express.Router();
const courseController = require("../Controllers/courseController");
const auth = require("../auth.js");



// Route for creating a course
router.post("/", auth.verify,courseController.addCourse);
//
router.get("/all", auth.verify, courseController.allCourses)
//
router.get("/allActive", courseController.allActiveCourses);

router.get("/allNotActive", auth.verify,courseController.allActiveCourses);

//route for retrieving details of specific course

//params routes
router.get("/:courseId", courseController.courseDetails);

router.put("/update/:courseId", auth.verify, courseController.updateCourse)

router.put("/:courseId/archive", auth.verify, courseController.archiveCourse)

module.exports = router;
